基于TP6.0的图鸟通用管理后台 
===============

> 运行环境要求PHP7.1+\
> 数据库mysql5.7+\
> redis5.0+\

## 主要新特性



## 安装

~~~
composer create-project topthink/think tp 6.0.*-dev
~~~

如果需要更新框架使用
~~~
composer update topthink/framework
~~~

安装了第三方的插件
~~~
composer require topthink/think-multi-app
composer require liliuwei/thinkphp-jump
composer require topthink/think-image:^1.0
composer require xemlock/htmlpurifier-html5
composer require zzstudio/think-auth
composer require lcobucci/jwt
composer require zoujingli/wechat-developer
composer require phpoffice/phpspreadsheet
composer require topthink/think-queue
~~~

## 文档

拿到该模版的时候，请修改.env下的相关信息\
修改config目录下的session.php和cache.php里面的prefix字段的信息\
将对应的域名添加到授权后台中，并换取验证码


## 参与开发


## 版权信息

版权所有Copyright © 2018-2020 by tuniao (http://www.tuniaokj.com)

All rights reserved。