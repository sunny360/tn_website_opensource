<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-09
 * Time: 09:12
 */

namespace app\admin\model\traits;


use think\facade\Cache;

trait ConfigBase
{
    protected static $have_img_type = [6,7,8,9];    //包含图片的类型
    protected static $have_values_type = [2,3,4];   //包含可选值的类型
    protected static $array_data_type = [3,8,9];    //传过来的数据是以数组的形式

    /**
     * 在保存多选值前将值进行判断和处理
     * @param $value
     * @param $data
     * @return string
     */
    public function setValuesAttr($value, $data)
    {
        if (empty($value) || !in_array($data['type'],self::$have_values_type)) {
            return '';
        } else {
            // 如果是需要用英文逗号进行数据分割的，将中文，变成英文，
            return convert_chinese_dot($value);
        }
    }

    /**
     * 在获取可选值前将其转换为数组
     * @param $value
     * @param $data
     * @return array|string
     */
    public function getValuesAttr($value, $data)
    {
        if (empty($value)) {
            return '';
        } else {
            if (in_array($data['type'],self::$have_values_type)) {
                return explode(',', $value);
            }
            return $value;
        }
    }

    /**
     * 在写入value值之前，判断对应的值是否为数组，如果是就将其json化
     * @param $value
     * @param $data
     * @return false|string
     */
    public function setValueAttr($value, $data)
    {
        if (empty($value)) {
            if (in_array($data['type'],self::$array_data_type)) {
                return json_encode([],JSON_UNESCAPED_UNICODE);
            }
            return '';
        } else {
            if (in_array($data['type'], self::$array_data_type)) {
                return json_encode($value, JSON_UNESCAPED_UNICODE);
            }
            return $value;
        }
    }

    /**
     * 获取value值之前，判断是否为json数据，如果是则进行反json化
     * @param $value
     * @param $data
     * @return mixed|string
     */
    public function getValueAttr($value, $data)
    {
        if (empty($value)) {
            return '';
        } else {
            if (in_array($data['type'], self::$array_data_type)) {
                if (is_json($value)) {
                    return json_decode($value, true);
                }
                return '';
            }
            return $value;
        }
    }

    /**
     * 获取对应配置项的值
     * @param $en_name
     * @param string $default
     * @return mixed|string
     */
    public static function getConfigValue($en_name, $default = '')
    {
        // 尝试从缓存中取出cache_key对应的数据。如果能取到，便直接返回数据。
        // 否则运行匿名函数中的代码来取出数据，返回的同时做了缓存
        $configValue = Cache::remember(self::SYSTEM_CONFIG_KEY,function () {
            return self::updateConfigValue();
        });
        if (is_array($configValue)) {
            return isset($configValue[$en_name]) ? $configValue[$en_name] : $default;
        } else {
            return !empty($configValue) ? $configValue : $default;
        }
    }

    /**
     * 处理添加新的配置项时对应类型的默认值
     * @param $data
     */
    protected static function handleAddDefaultValue($data)
    {
        // 如果类型是复选框或者下拉菜单，也需要将值中的中文，变成英文，
        if ($data['type'] == 3 || $data['type'] == 4) {
            $data['value'] = convert_chinese_dot($data['value']);
            if ($data['type'] == 3 && !empty($data['value'])) {
                $data['value'] = json_encode(explode(',',$data['value']),JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * 处理修改配置项时类型发生了改变的情况
     * @param $old_data
     * @param $new_data
     */
    protected static function handleEditChangeType($old_data, &$new_data)
    {
        if ($old_data['type'] != $new_data['type']) {
            $new_data['value'] = '';
            // 判断之前是否为可选值，但是换成了非可选值类型
            if (in_array($old_data['type'], self::$have_values_type) && !in_array($new_data['type'], self::$have_values_type)) {
                $new_data['values'] = '';
            }

            // 判断被替换类型的数据中是否包含富文本或者附件，如果有则进行删除操作
            $needDeleteFile = [];
            switch($old_data['type']) {
                case 6:
                    $needDeleteFile = self::getRichTextImage($old_data['value']);
                    break;
                case 9:
                    foreach ($old_data['value'] as $item) {
                        $needDeleteFile[] = $item['url'];
                    }
                    break;
            }

            delFile($needDeleteFile);
        }
    }

    /**
     * 处理被删除的配置项
     * @param $data
     */
    protected static function handleDeleteFile($data)
    {
        if (empty($data) || !is_array($data)) {
            return ;
        }
        $needDeleteFile = [];
        foreach ($data as &$item) {
            if (!empty($item['value'])) {
                switch ($item['type']) {
                    case 6:
                        $needDeleteFile = array_merge($needDeleteFile, self::getRichTextImage($item['value']));
                        break;
                    case 9:
                        $delete_file = [];
                        foreach ($item['value'] as $file_item) {
                            $delete_file[] = $file_item['url'];
                        }
                        $needDeleteFile = array_merge($needDeleteFile, $delete_file);
                        break;
                }
            }
            $item['value'] = '';
        }

        // 将对应的值重置为空
        $result = (new static())->saveAll($data);

        if ($result !== false) {
            delFile($needDeleteFile);
        }
    }

    /**
     * 更新配置项的值
     * @return array
     */
    protected static function updateConfigValue()
    {
        $configValue = [];
        $configData = static::getDataWithField([['status','=',1]],['en_name','type','value']);
        $configData = !empty($configData) ? $configData->toArray() : [];

        foreach ($configData as $item) {
            if (empty($item['en_name'])) {
                continue;
            }
            $configValue[$item['en_name']] = $item['value'];
        }

        // 将取到的配置项信息放入缓存中
        Cache::set(self::SYSTEM_CONFIG_KEY,$configValue,0);

        return $configValue;
    }
}