<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-05
 * Time: 08:36
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class AuthRule extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'pid' => 'require|number',
        'name' => 'require|max:100|unique:AuthRule',
        'title' => 'require|max:20',
        'type' => 'require|number',
        'condition' => 'max:255',
        'public_rule' => 'require|number|between:0,1',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'pid.require' => 'pid不能为空',
        'pid.number' => 'pid必须为整数',
        'name.require' => '规则标识不能为空',
        'name.max' => '规则标识最大不能超过100',
        'name.unique' => '规则标识不能重复，请检查后再次提交',
        'title.require' => '规则标题不能为空',
        'title.max' => '规则标题最大不能超过20',
        'type.require' => '规则验证方式不能为空',
        'type.number' => '规则验证方式的值格式不正确',
        'condition.max' => '多重验证规则最大不能超过255',
        'public_rule.require' => '链接不能为空',
        'public_rule.number' => '链接格式不正确',
        'public_rule.between' => '链接格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序格式不正确',
        'sort.gt' => '排序格式不正确',
        'status.require' => '状态不能为空',
        'status.number' => '状态格式不正确',
        'status.between' => '状态格式不正确',
    ];

    protected $scene = [
        'add' => ['pid','name','title','type','condition','auth_open','sort','status'],
        'edit' => ['id','pid','name','title','type','condition','auth_open','sort','status'],
    ];
}