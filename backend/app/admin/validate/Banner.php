<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 13:59
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class Banner extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:60',
        'image' => 'require|max:255',
        'pos_id' => 'require|number|gt:0',
        'article_id' => 'require|number',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '轮播图名称不能为空',
        'title.max' => '轮播图名称最大不能超过60',
        'image.require' => '轮播图片不能为空',
        'image.max' => '轮播图片最大长度不能超过255',
        'pos_id.require' => '轮播位不能为空',
        'pos_id.number' => '轮播位格式不正确',
        'pos_id.gt' => '轮播位格式不正确',
        'article_id.require' => '所属文章不能为空',
        'article_id.number' => '所属文章格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序的值必须为正整数',
        'sort.gt' => '排序的值必须大于0',
        'status.require' => '是否显示不能为空',
        'status.number' => '是否显示格式不正确',
        'status.between' => '是否显示格式不正确',
    ];

    protected $scene = [
        'add' => ['title','image','pos_id','article_id','sort','status'],
        'edit' => ['id','title','image','pos_id','article_id','sort','status'],
    ];
}