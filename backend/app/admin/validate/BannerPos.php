<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 14:18
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class BannerPos extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:60|unique:BannerPos'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '轮播位名称不能为空',
        'title.max' => '轮播位名称最大长度不能超过60',
        'title.unique' => '轮播位名称不能重复',
    ];

    protected $scene = [
        'add' => ['title'],
        'edit' => ['id','title'],
    ];
}