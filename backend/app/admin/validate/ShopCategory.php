<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-31
 * Time: 09:45
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ShopCategory extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:100',
        'sort' => 'require|number|gt:0',
        'status' => 'number|between:0,1',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序值不正确',
        'sort.gt' => '排序值不正确',
        'status.number' => '推荐值格式不正确',
        'status.between' => '推荐值格式不正确'
    ];

    protected $scene = [
        'add' => ['title', 'sort', 'status'],
        'edit' => ['id', 'title', 'sort', 'status']
    ];
}