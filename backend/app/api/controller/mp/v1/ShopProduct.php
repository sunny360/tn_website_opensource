<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-02
 * Time: 09:34
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\ShopProduct as ShopProductModel;

class ShopProduct extends BaseController
{
    /**
     * 根据id获取商品数据
     * @http get
     * @url /shop_product/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->param('id', 0);

        $data = ShopProductModel::getShowDataByID($id);

        return tn_yes('获取商品数据成功', ['data' => $data]);
    }

    /**
     * 更新商品的sku数据
     * @http get
     * @url /shop_product/get_sku_data
     * @return \think\response\Json
     */
    public function getProductSkuData()
    {
        $id = $this->request->param('id', 0);

        $data = ShopProductModel::getShowDataByID($id);

        return tn_yes('获取商品sku数据成功', ['data' => $data['sku_data']]);
    }

    /**
     * 获取指定规格的库存数据
     * @http get
     * @url /shop_product/get_specs_stock
     * @return \think\response\Json
     */
    public function getProductSpecsStock()
    {
        $specs_id = $this->request->get('specs_id', '');

        $data = ShopProductModel::getSpecsStockData($specs_id);

        return tn_yes('获取规格库存数据成功', ['data' => $data]);
    }
}