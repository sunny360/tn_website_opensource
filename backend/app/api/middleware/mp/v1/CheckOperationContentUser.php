<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-04
 * Time: 13:33
 */

namespace app\api\middleware\mp\v1;

use app\common\exception\BusinessException;
use app\common\model\Content as ContentModel;

class CheckOperationContentUser
{
    public function handle($request, \Closure $next)
    {
        // 判断是否没有登陆用户的分享
        if ($request->param('add_user')) {
            if (!ContentModel::checkOperationUser([
                'type' => $request->param('type'),
                'id' => $request->param('id'),
            ])) {
                throw new BusinessException([
                    'msg' => '已经对该内容进行过相同操作',
                    'errorCode' => 40104,
                ]);
            }
        }

        return $next($request);
    }
}