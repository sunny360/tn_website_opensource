<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-04
 * Time: 10:05
 */

namespace app\api\middleware\mp\v1;


use app\common\exception\LoginException;
use app\common\service\Token;
use app\common\model\Order as OrderModel;

class CheckOrderBelongsToUser
{
    public function handle($request, \Closure $next)
    {
        // 判断用户是否已经登录，并且已经存在token
        $uid = Token::getCurrentTokenVar('uid');

        if (empty($uid)) {
            throw new LoginException([
                'code' => 401,
                'msg' => '用户还未授权或者登录',
                'errorCode' => 60101
            ]);
        }

        // 检查当前订单id是否数据当前登录用户
        OrderModel::checkOrderIsBelongsToUser($request->param('id', 0), $uid);

        return $next($request);
    }
}