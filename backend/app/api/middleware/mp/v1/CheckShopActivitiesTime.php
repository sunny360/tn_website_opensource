<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-12
 * Time: 10:20
 */

namespace app\api\middleware\mp\v1;


use app\common\exception\ParameterException;
use app\common\exception\ShopException;
use app\common\model\ShopActivities;

class CheckShopActivitiesTime
{
    public function handle($request, \Closure $next)
    {
        // 判断是否当前活动是否存在或者开启
        if ($request->param('activities_id')) {
            $status = ShopActivities::checkActivitiesTimeRange($request->param('activities_id'));
            switch ($status) {
                case -1:
                    throw new ShopException([
                        'code' => 404,
                        'errorCode' => 40204,
                        'msg' => '微信小商店订单活动已经结束或者不存在'
                    ]);
                    break;
                case -2:
                    throw new ShopException([
                        'code' => 400,
                        'errorCode' => 40206,
                        'msg' => '微信小商店订单活动还未开始'
                    ]);
                    break;
                case -3:
                    throw new ShopException([
                        'code' => 400,
                        'errorCode' => 40207,
                        'msg' => '微信小商店订单活动已经结束'
                    ]);
                    break;
            }
        } else {
            throw new ParameterException();
        }

        return $next($request);
    }
}