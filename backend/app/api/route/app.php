<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 12:17
 */
declare(strict_types = 1);

use think\facade\Route;

Route::group(function () {
    Route::group(':app/:version', function () {

        Route::group(function () {
            // 轮播图管理
            Route::group('banner', function () {
                Route::get(':pos_id/:limit',':app\:version\Banner@getBanner'); // 根据pos_id获取轮播图数据
            });

            // 栏目管理
            Route::group('category', function () {
                Route::get('get_nav/:top_id',':app\:version\Category@getNavData');
            });

            // 配置参数管理
            Route::group('config/wx', function () {
                Route::get('/',':app\:version\WxConfig@getConfigValue');
            });

            // 搜索管理
            Route::group('search', function () {
                Route::get('get_result',':app\:version\Search@getList');
            });

            // 业务管理
            Route::group('business', function () {
                Route::get('get_title/:limit',':app\:version\Business@getBusinessTitle');
                Route::get('get_id/:id',':app\:version\Business@getBusinessData');
                Route::post('update_advisory/:id',':app\:version\Business@updateAdvisoryUser')->middleware('check.business.operation');
            });

            // 内容管理
            Route::group('content', function () {
                Route::get('get_category_list',':app\:version\Content@getCategoryPaginate');
                Route::get('get_recomm/:type/:limit',':app\:version\Content@getRecommData');
                Route::get('get_newest/:type/:limit',':app\:version\Content@getNewestData');
                Route::get('get_data/:type/:id',':app\:version\Content@getContentData');
                Route::post('update_operation/:type/:id', ':app\:version\Content@updateOperationUser')->middleware('check.content.operation');
            });

            // 图鸟商店商品管理
            Route::group('shop_product', function () {
                Route::get('get_id/:id', ':app\:version\ShopProduct@getByID');
                Route::get('get_sku_data/:id', ':app\:version\ShopProduct@getProductSkuData');
                Route::get('get_specs_stock', ':app\:version\ShopProduct@getProductSpecsStock');
            });

            // 图鸟商店订单管理
            Route::group('tn_shop_order', function () {
                Route::delete('close', ':app\:version\Order@closeTNShopOrder')->middleware(['check.order.belongs.user']);
                Route::delete('delete', ':app\:version\Order@deleteTNShopOrder')->middleware(['check.order.belongs.user']);
                Route::delete('close_refund_application', ':app\:version\Order@closeTNShopRefundApplication')->middleware(['check.order.belongs.user']);
                Route::post('confirm_receipt', ':app\:version\Order@confirmReceiptTNShopOrder')->middleware(['check.order.belongs.user']);
                Route::post('launch_order_refund', ':app\:version\Order@launchTNShopOrderRefund')->middleware(['check.order.belongs.user']);
                Route::post('set_delivery_subscribe',':app\:version\Order@setAllowDeliverySubscribe');
                Route::get('get_list', ':app\:version\Order@getTNShopOrderList');
                Route::get('get_by_id', ':app\:version\Order@getTNShopOrderDetail')->middleware(['check.order.belongs.user']);
            });

            // 支付管理
            Route::group('pay', function () {
                Route::post('pre_appreciate_order', ':app\:version\Order@preCreateAppreciateOrder');
                Route::post('pre_tn_shop_order',':app\:version\Order@preCreateTNShopOrder');
                Route::post('pay_tn_shop_order', ':app\:version\Order@payTNShopOrder')->middleware(['check.order.belongs.user']);
                Route::delete('cancel_order', ':app\:version\Order@cancelOrder');
                Route::delete('cancel_tn_shop_order', ':app\:version\Order@cancelTNShopOrder');
            });

            // Token令牌管理
            Route::group('token', function () {
                Route::post('get',':app\:version\Token@getToken');    // 获取token令牌
                Route::post('verify',':app\:version\Token@verifyToken');  // 验证token令牌
            });

            // 用户管理
            Route::group('user', function () {
                Route::get('exist',':app\:version\User@checkUserExist'); // 更新用户信息
                Route::put('update',':app\:version\User@updateUserInfo'); // 更新用户信息
            });

        })->middleware(['check.api.authority']);
//        });

        Route::group(function () {
            // 微信小程序接收消息推送
            Route::group('mp_notify', function () {
                Route::post('message_push', ':app\:version\WXMPNotify@ReceiveMessagePush');
                Route::get('message_push', ':app\:version\WXMPNotify@VerifyMessagePushService');
            });
        });

        Route::group(function () {
            // 微信公众号登录
            Route::group('login', function () {
                Route::get('admin_auth', ':app\:version\WeChatUserLogin@adminAuth');
            });
        });
    });
})->prefix('\\app\\api\\controller\\')
    ->allowCrossDomain([
        'Access-Control-Allow-Headers' => 'Token, Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With',
    ]);