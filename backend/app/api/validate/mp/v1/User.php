<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 19:23
 */

namespace app\api\validate\mp\v1;


use app\common\validate\BaseValidate;

class User extends BaseValidate
{
    protected $rule = [
        'nick_name' => 'require|max:100',
        'avatar_url' => 'max:255',
        'gender' => 'require|number|between:0,2',
    ];

    protected $message = [
        'nick_name.require' => '用户名不能为空',
        'nick_name.max' => '用户名最大长度为100',
        'avatar_url.max' => '头像地址最大长度为155',
        'gender.require' => '性别不能为空格',
        'gender.number' => '性别的值必须为整型',
        'gender.between' => '性别的值在0到2之间',
    ];
}