<?php


namespace app\common\enum;


class ScopeEnum
{
    //游客
    const GUEST = 1;

    //已认证用户
    const USER = 16;

    //管理员用户
    const ADMIN = 32;
}