<?php


namespace app\common\exception;


class BusinessException extends BaseException
{
    public $code = 404;
    public $msg = '对应的业务内容不存在';
    public $errorCode = 40010;
}