<?php


namespace app\common\exception;


class ContentException extends BaseException
{
    public $code = 404;
    public $msg = '对应id的内容为空';
    public $errorCode = 40100;
}