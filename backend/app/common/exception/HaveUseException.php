<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-07
 * Time: 11:02
 */

namespace app\common\exception;


class HaveUseException extends BaseException
{
    public $code = 403;
    public $msg = '被删除的数据还在使用';
    public $errorCode = 10003;
}