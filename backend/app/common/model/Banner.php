<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 14:20
 */

namespace app\common\model;


use app\common\exception\BannerException;
use app\common\exception\ParameterException;
use think\model\concern\SoftDelete;
use app\admin\validate\Banner as Validate;
use app\api\validate\mp\v1\Banner as ApiBannerValidate;

class Banner extends BaseModel
{
    protected $hidden = ['create_time','update_time','delete_time'];

    // 使用软删除
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    public function pos()
    {
        return $this->belongsTo('\\app\\admin\\model\\BannerPos','pos_id','id')
            ->field(['id','title']);
    }

    public function getImageAttr($value, $data)
    {
        return [
            'value' => $value,
            'prefix' => $this->prefixImgUrl($value)
        ];
    }

    /**
     * 获取轮播图的分页数据
     * @param array $params
     * @return \think\Paginator
     */
    public static function getPaginationList(array $params)
    {
        static::validatePaginationData($params);

        $static = new static();

        $static = $static->order(['pos_id'=>'ASC','sort'=>'ASC']);

        $static = $static->with(['pos']);

        foreach ($params as $name => $value) {
            $value = !is_array($value) ? trim($value) : $value;
            switch ($name) {
                case 'title':
                    if (!empty($value)) {
                        $like_text = '%' . $value . '%';
                        $static = $static->whereLike('title', $like_text);
                    }
                    break;
                case 'pos_id':
                    if (!empty($value)) {
                        $static = $static->where('pos_id', '=', intval($value));
                    }
                    break;
                case 'sort_order':
                    if (!empty($value)) {
                        $static = $static->order($params['sort_prop'], $value == 'descending' ? 'desc' : 'asc');
                    }
                    break;
            }
        }

        return $static
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit']
            ], false);
    }

    /**
     * 根据轮播位id获取指定数量的轮播图信息
     * @param $params
     * @return array
     */
    public static function getBannerByPosID($params)
    {
        $validate = new ApiBannerValidate();
        if (!$validate->check($params)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $banner_data =  static::where([['pos_id','=',$params['pos_id']], ['status','=',1]])
            ->limit($params['limit'])
            ->order('sort', 'asc')
            ->select();

        if ($banner_data->isEmpty()) {
            throw new BannerException();
        }

        return $banner_data->toArray();

    }

    /**
     * 添加轮播图信息
     * @param array $data
     * @return bool
     */
    public static function addBanner(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('add')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::create($data);

        if (isset($static->id) && $static->id > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 编辑轮播图信息
     * @param array $data
     * @return bool
     */
    public static function editBanner(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('edit')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::find($data['id']);
        $result = $static->allowField(['id','title','pos_id','article_id','image','sort','status'])
            ->save($data);

        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }
}