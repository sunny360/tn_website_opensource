<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-18
 * Time: 13:15
 */

namespace app\common\service;


use think\facade\Filesystem;
use think\File;

class Upload
{
    // 上传文件的类型
    // 文件类型上传的图片
    public const TYPE_IMAGE = 'image';
    // 富文本类型上传的图片
    public const TYPE_RICH_TEXT = 'rich_text';
    // 文件类型上传的文件
    public const TYPE_FILE = 'file';

    // 上传文件的方式
    // 保存到图集中
    public const SAVE_TO_IMG_LIST = 'img_list';
    // 保存到对应的文件夹中
    public const SAVE_TO_DIR = 'to_dir';


    /**
     * 保存上传的文件
     * @param $file 文件信息
     * @param string $upload_type 上传文件的方式
     * @param string $dir_name 上传文件所保存的位置(文件夹)
     * @return array
     */
    public static function saveFile($file, string $upload_type, string $dir_name = 'img_list')
    {
        $save_convert = 'md5';
        $ext = null;
        $save_path = null;
        $sha1 = null;
        $md5 = null;
        $size = null;
        $origin_name = null;

        // 判断上传的方式
        if ($upload_type === self::SAVE_TO_DIR) {
            $save_convert = 'tn_file';
        }

        // 判断是否为多文件
        if (is_array($file)) {
            foreach ($file as $key => $item) {
                $save_info = self::saveFileToDisk($item, 'uploads/' . $dir_name, $save_convert);

                // 保存文件信息
                $ext[$key] = $save_info['ext'];
                $save_path[$key] = $save_info['save_path'];
                $sha1[$key] = $save_info['sha1'];
                $md5[$key] = $save_info['md5'];
                $size[$key] = $save_info['size'];
                $origin_name[$key] = $save_info['origin_name'];
            }
        } else {
            $save_info = self::saveFileToDisk($file, 'uploads/' . $dir_name, $save_convert);

            // 保存文件信息
            $ext = $save_info['ext'];
            $save_path = $save_info['save_path'];
            $sha1 = $save_info['sha1'];
            $md5 = $save_info['md5'];
            $size = $save_info['size'];
            $origin_name = $save_info['origin_name'];
        }

        return [
            'ext' => $ext,
            'save_path' => $save_path,
            'sha1' => $sha1,
            'md5' => $md5,
            'size' => $size,
            'origin_name' => $origin_name
        ];
    }

    /**
     * 保存文件到硬盘中
     * @param File $file
     * @param string $path
     * @param string $save_convert
     * @return array
     */
    private static function saveFileToDisk(File $file, string $path, string $save_convert = '')
    {
        if ($save_convert === 'tn_file') {
            // 所有上传的文件都保存在 public/storage/uploads/[dirName] 目录里
            $save_name = Filesystem::disk('public')->putFile($path, $file, function () use ($file) {
                return date('Ymd') . DIRECTORY_SEPARATOR . md5(session_create_id()); // php 7.1新函数
            });
        } else {
            // 所有上传的文件都保存在 public/storage/uploads/[dirName] 目录里
            $save_name = Filesystem::disk('public')->putFile($path, $file, $save_convert);
        }


        // 返回文件信息
        return [
            'ext' => $file->extension(),
            'save_path' => '/storage/' . $save_name,
            'sha1' => $file->hash('sha1'),
            'md5' => $file->hash('md5'),
            'size' => $file->getSize(),
            'origin_name' => $file->getOriginalName(),

        ];
    }
}