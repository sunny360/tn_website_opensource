<?php
/**
 * tuniao.php
 * @author Jaylen
 */

function tn_yes(string $msg = '', array $options = [], int $errorCode = 0,int $code = 200)
{
    return tn_api($msg, $options, $errorCode, $code);
}
function tn_no(string $msg = '', array $options = [], int $errorCode = 999,int $code = 400)
{
    return tn_api($msg, $options, $errorCode, $code);
}
function tn_api(string $msg = '', array $options = [], int $errorCode = 0, int $code = 200)
{
    return returnJsonResult($msg, $options,$errorCode, $code);
}
function returnJsonResult(string $msg = '',array $options = [], int $errorCode = 0, int $code = 200)
{
    // 拼接数组
    $data = array_merge($options, [
        'msg' => $msg,
        'errorCode' => $errorCode
    ]);
    return json($data, $code);
}