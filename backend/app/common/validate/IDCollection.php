<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-16
 * Time: 19:14
 */

namespace app\common\validate;


class IDCollection extends BaseValidate
{
    protected $rule = [
        'ids' => 'require|checkIDs',
    ];

    protected $message = [
        'ids.require'   =>  'id值不能为空',
        'ids.checkIDs'  =>  'id值的格式不对',
    ];
}