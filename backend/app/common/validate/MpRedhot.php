<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-22
 * Time: 10:35
 */

namespace app\common\validate;


class MpRedhot extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'name' => 'max:20',
        'url' => 'require|max:255'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'name.max' => '红包名称最大为20',
        'url.require' => '红包领取链接不能为空',
        'url.max' => '红包领取链接最大为255',
    ];

    protected $scene = [

    ];
}