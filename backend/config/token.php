<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-10
 * Time: 19:46
 */
declare(strict_types = 1);

return [
    // jwt token相关设置
    // 过期时间
    'jwt_token_expire' => 7200,
    // 密钥字符串
    'jwt_token_secret' => 'u5S_&a3c9TUu!Z',
    // 标识前缀
    'jwt_token_id_prefix' => 'tuniao'
];