<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-09-30
 * Time: 09:34
 */

return [
    'component_access_token_url' => 'https://api.weixin.qq.com/cgi-bin/component/api_component_token',  //微信第三方平台获取component_access_token的url地址
    'pre_auth_code_url' => 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=%s', // 微信第三方平台获取pre_auth_code的url地址
    'authorizer_refresh_token_url' => 'https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=%s', // 微信第三方平台获取authorizer_refresh_token的地址
    'authorizer_access_token_url' => 'https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=%s', // 微信第三方平台获取authorizer_access_token的url地址
    'scan_access_code_url' => 'https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=%s&pre_auth_code=%s&redirect_uri=%s', // 授权注册页面扫码授权的url地址
    'oc_page_auth_code_url' => 'https://mp.weixin.qq.com/safe/bindcomponent'.
        '?action=bindcomponent&auth_type=3&no_scan=1&component_appid=%s&pre_auth_code=%s&redirect_uri=%s#wechat_redirect'   // 授权注册页面点击授权的url地址
];