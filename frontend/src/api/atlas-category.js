import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function listAtlasCategory(params) {
  return request({
    url: api_prefix + 'atlas_category/list',
    method: 'get',
    params
  })
}

export function getAtlasCategoryByID(id) {
  return request({
    url: api_prefix + 'atlas_category/get_id',
    method: 'get',
    params: { id }
  })
}

export function getAllAtlasCategoryName() {
  return request({
    url: 'atlas_category/get_all_name',
    method: 'get'
  })
}

export function addAtlasCategory(data) {
  return request({
    url: api_prefix + 'atlas_category/add',
    method: 'post',
    data
  })
}

export function editAtlasCategory(data) {
  return request({
    url: api_prefix + 'atlas_category/edit',
    method: 'put',
    data
  })
}

export function updateAtlasCategory(data) {
  return request({
    url: api_prefix + 'atlas_category/update',
    method: 'put',
    data
  })
}

export function deleteAtlasCategory(ids) {
  return request({
    url: api_prefix + 'atlas_category/delete',
    method: 'delete',
    data: { ids }
  })
}
