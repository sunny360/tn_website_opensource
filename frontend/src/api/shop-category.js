import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getCategoryList(params) {
  return request({
    url: api_prefix + 'shop_category/list',
    method: 'get',
    params
  })
}

export function getCategoryByID(id) {
  return request({
    url: api_prefix + 'shop_category/get_id',
    method: 'get',
    params: { id }
  })
}

export function getAllCategoryTitle() {
  return request({
    url: api_prefix + 'shop_category/get_all_title',
    method: 'get'
  })
}

export function getAllCount() {
  return request({
    url: api_prefix + 'shop_category/get_all_count',
    method: 'get'
  })
}

export function addCategory(data) {
  return request({
    url: api_prefix + 'shop_category/add',
    method: 'post',
    data
  })
}

export function editCategory(data) {
  return request({
    url: api_prefix + 'shop_category/edit',
    method: 'put',
    data
  })
}

export function updateCategory(data) {
  return request({
    url: api_prefix + 'shop_category/update',
    method: 'put',
    data
  })
}

export function deleteCategory(ids) {
  return request({
    url: api_prefix + 'shop_category/delete',
    method: 'delete',
    data: { ids }
  })
}
