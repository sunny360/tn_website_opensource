module.exports = {

  title: '图鸟科技管理后台',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether need tagsView
   */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true,

  /**
   * 微信网站应用登录授权appid
   * @type {String} ''
   */
  wxAuthLoginAPPID: 'wx776a45b181603d74'
}
