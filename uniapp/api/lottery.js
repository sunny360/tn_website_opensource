import { request } from '@/utils/request'

export function getLotteryData(id) {
  return request({
    url: "lottery/get_by_id",
    method: 'get',
    data: {
      id
    }
  })
}

export function getUserLotteryPrize(id) {
  return request({
    url: 'lottery/get_prize',
    method: 'post',
    data: {
      id
    }
  })
}
