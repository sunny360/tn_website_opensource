import { request } from '@/utils/request'

export function preOrder(data) {
  return request({
    url: 'pay/pre_appreciate_order',
    method: 'post',
    data: data
  })
}

export function cancelOrder(data) {
  return request({
    url: 'pay/cancel_order',
    method: 'delete',
    data: data
  })
}