import { request } from '@/utils/request'

export function getProductDetailByID(id) {
  return request({
    url: 'shop_product/get_id/' + id,
    method: 'get'
  })
}

export function getProductSpecsData(id) {
  return request({
    url: 'shop_product/get_sku_data/' + id,
    method: 'get'
  })
}

export function getProductStockData(data) {
  return request({
    url: 'shop_product/get_specs_stock',
    method: 'get',
    data
  })
}
