export const BASE_URL = 'http://website_open.tuniaokj.com/'

export const MP_API_URL = BASE_URL + 'api.php/mp/v1/'

export const TOKEN_NAME = 'tn_website_open_token'

export const SHOPPING_CART_STORAGE_KEY = 'tn_website_open_shop_shopping_cart'
export const ORDER_STORAGE_KEY = 'tn_website_open_shop_order'